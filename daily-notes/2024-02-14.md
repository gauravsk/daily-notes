#### Hi! Today is Wednesday, 14 February

------------------------

## Reflections on yesterday
#### How I spent time yesterday
- Ended up treating it mostly as a day for personal work

#### Reflections on meetings yesterday:
- Personal meetings

#### Links/Emails/Thoughts from yesterday to follow up on:
- Admin email (receipts)
- 

## Thematic priorities for today

#### Priority 1
- Start on Table of predictions that people have made about PSF x Global Change

#### Priority 2

## Meetings today
- Meeting with Ana

## Tasks I *need* to finish today


## Tasks that would be *nice to* to finish today
- Table for Gyuri and Po-Ju

## Tasks on my radar that are unlikely to happen today


## Wishes for today
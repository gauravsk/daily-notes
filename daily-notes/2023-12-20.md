#### Hi! Today is Wednesday, 20 December

------------------------

## Reflections on yesterday
#### How I spent time yesterday
Lots of play time with dogs :) 
Did some work on [lab website](https://gklab.org/documents)

#### Reflections on meetings yesterday:
Met with Ameya - he's on a good track

#### Links/Emails/Thoughts from yesterday to follow up on:
Check with Lauren about how Ameya's work fits in with things


## Thematic priorities for today

#### Priority 1
Continue tweaking the website?
#### Priority 2
Jot down thoughts about teaching in [[notes-for-f24ecology]]

## Meetings today

none
## Tasks I *need* to finish today


## Tasks that would be *nice to* to finish today

- Replies to holiday-type messages

## Wishes for today
A good run with Henry!
#### Hi! Today is Tuesday, 13 August

------------------------

## Reflections on yesterday
#### How I spent time yesterday
- Onboarding Edmarie
- Meeting with Richard 
- Revised IRB submission (recheck this today)

#### Reflections on meetings yesterday:
- Think more about potential trait chapter for Richard. 
- Also look into mycorrhizal isolation protocol
#### Links/Emails/Thoughts from yesterday to follow up on:
- PhD student interest emails

## Thematic priorities for today

#### Priority 1
- Welcome Rachana to BR

#### Priority 2

## Meetings today
- Robin - follow up on saber

## Tasks I *need* to finish today
- [x]  ICTS information form

## Tasks that would be *nice to* to finish today


## Tasks on my radar that are unlikely to happen today
- [ ] Review request 

## Wishes for today
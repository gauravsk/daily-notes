#### Hi! Today is Friday, 1 December

------------------------

## Reflections on yesterday
#### How I spent time yesterday
- Mostly worked on Vicks application
- Spoke with Richard after his final exam, looking forward to next semester

#### Reflections on meetings yesterday:
- Mostly reflections on student meetings -- excited to see what they put together for their project. 

#### Links/Emails/Thoughts from yesterday to follow up on:


## Thematic priorities for today

#### Priority 1 
- Submit Vicks app

#### Priority 2
- Final day of teaching this semester!

## Meetings today
- Li Lab meeting
- Teaching
- Ecologists Lunch

## Tasks I *need* to finish today
Submit Vicks app!


## Tasks that would be *nice to* to finish today


## Tasks on my radar that are unlikely to happen today


## Wishes for today
Dry bike ride to and from campus
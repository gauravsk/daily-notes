#### Hi! Today is Wednesday, 31 January

------------------------

## Reflections on yesterday
#### How I spent time yesterday
- Mostly working on plant competition X microbes section
- Met with grad recruit - notes in private notebook
- Met with Po-Ju about Response Phase time question

#### Reflections on meetings yesterday:
- Potential for a short paper about the response phase time - seems like it would be best for this to come out before spsf meta-analysis, if we want to use those ideas there. 
#### Links/Emails/Thoughts from yesterday to follow up on:


## Thematic priorities for today

#### Priority 1
- Continue polishing PSF-x-competition section

#### Priority 2
- Draft of lab guidelines book

## Meetings today
- Meeting with another grad recruit
- Faculty meeting 

## Tasks I *need* to finish today
- ESA Div C application due (Tomorrow?)

## Tasks that would be *nice to* to finish today


## Tasks on my radar that are unlikely to happen today
- Write Richard evaluation summary to share with him
- Work on table for plant-microbe interactions global change project
- Send Neha response to Biograds email
- Buy chairs from Unviersity surplus store
- Email Evanna about faculty committee

## Wishes for today
#### Hi! Today is Monday, 3 February

------------------------

## Reflections from yesterday

https://new.nsf.gov/executive-orders

## Reflections for today

### *Thematic priorities* 

Try to make progress on Grant documents and Fastplants tasks
### *Meetings*

Lots of individual meetings -- but struggling a bit to ensure that these will be maximally productive

## Tasks

### Due today

```tasks
due on 2025-02-03
sort by function task.file.folder
```


### Due soon

```tasks
due after today
due before next 7 days
```
### On the docket
`
```tasks
not done
due after this week
```

### Accomplished today

```tasks
done on 2025-02-03
```


## Notes from meetings


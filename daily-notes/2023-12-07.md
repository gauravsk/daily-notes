#### Hi! Today is Thursday, 7 December

------------------------

## Reflections on yesterday
#### How I spent time yesterday
- Wasn't able to focus very well yesterday, but spent time primarily on cleaning up gradebook. 

#### Reflections on meetings yesterday:
- Met with Maria in College of Science about budget stuff. Good to finally get account numbers etc. 

#### Links/Emails/Thoughts from yesterday to follow up on:
- Email Elizabeth about having acct number for past orders changed to restricted funds
- Email Elizabeth and Lori to get info about hiring postdoc process
- **Send spending plan by Jan 11th**

## Thematic priorities for today

#### Priority 1
Grade all submitted semester projects and organize the gradebook

#### Priority 2
Meet with undergrads as an end-of-semester checkin

## Meetings today
- Meetings with Milayna and Swati


## Tasks I *need* to finish today


## Tasks that would be *nice to* to finish today


## Tasks on my radar that are unlikely to happen today


## Wishes for today
#### Hi! Today is Tuesday, 25 June

------------------------

## Reflections on yesterday
#### How I spent time yesterday
- Reading a bit more about $I_C$
- Setting up balance etc
- 
#### Reflections on meetings yesterday:

#### Links/Emails/Thoughts from yesterday to follow up on:


## Thematic priorities for today

#### Priority 1

#### Priority 2

## Meetings today


## Tasks I *need* to finish today


## Tasks that would be *nice to* to finish today


## Tasks on my radar that are unlikely to happen today


## Wishes for today
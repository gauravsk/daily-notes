#### Hi! Today is Thursday, 23 November

------------------------

## Reflections on yesterday
#### How I spent time yesterday
- Writing the semester project guidelines took a long time - about 4 hours in total to get it all down and send out the email
- I spent about an hour working with Maddi to finalize the S. partitus paper - and already got positive responses back! 
- Got back home at about 5pm and then wasted a lot of time - the dangers of sitting on a couch :( 

#### Reflections on meetings yesterday:
No meetings!

#### Links/Emails/Thoughts from yesterday to follow up on:
Found a bunch of useful lab websites/codes of conduct etc. that I added to the [[Lab policies]] document.

## Thematic priorities for today

#### Priority 1
- Vicks application - try to have a first draft of two "action" paragraphs and maybe a more complete draft of the overview 
- start a file on [[equity in computational biol]] to collect resources for data on racial, gender, and other gaps in the field

## Meetings today
None professionally 

## Tasks I *need* to finish today
None.

## Tasks that would be *nice to* to finish today
- ~~complete paperwork for SEE seminar reimbursement~~ 
- ~~schedule send to undergrad student email - research interest.~~

## Tasks on my radar that are unlikely to happen today
- writing the paper Review still remains on the to-do list 

## Wishes for today
- I should start writing up end-of-year emails to send mentors, collaborators, friends
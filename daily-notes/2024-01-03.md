#### Hi! Today is Wednesday, 3 January

------------------------



## Thematic priorities for today

#### Priority 1
- Give comments on manuscripts from before break

#### Priority 2
- Work on weekly semester plan. v1

## Meetings today
- Talk with Maddi about research

## Tasks I *need* to finish today
- Contact student about hiring paperwork
## Tasks that would be *nice to* to finish today
- Journal editorial work

## Tasks on my radar that are unlikely to happen today
- Paper review 
--------------

### Notes for call with Po-Ju
- Fig 1... I keep wanting to compare how the same plant does in different soils, across the different treatments. This hard to do with current orientation (treatments in columns, species in rows) -- can we switch to species in columns instead? (Also aesthetically, the for being Arabidposis is bothering me lol)
	- Also, I'm kind of confused by what are the orange points in this figure. If they are plants growing in "BLANK" pots (sterilized potting mix only), my interpretation of the dataset leads to a slightly different plot:
	- ![[pics/Screenshot from 2024-01-03 12-38-03.png]]

**Note** that in the plot above, PLER has a huge low outlier in the delay treatment, but it's missing in the ms Fig 1. (I think I made the large black points above with Mean instead of Median, but the outlier points shouldn't be affected in any way...)

- Show Fig 4 as NMDS instead of/with stacked barplots? Not clear that there is a huge value to all the taxon names there. Also, was the "Fieldr" soil sampled at all the different time points (just before start of Immediate Response; just before start of Delayed Response) in addition to sample taking before any conditioning took place
  
-  Fig 3: Can we us something apart from green in bottom panel, to avoid confusion

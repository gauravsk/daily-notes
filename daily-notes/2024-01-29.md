#### Hi! Today is Monday, 29 January

------------------------

## Reflections on yesterday
#### How I spent time yesterday
- Mostly things around the hosue
- Wrote up a simulation of how response phase duration matters in PSF studies [link](https://gitlab.com/gauravsk/reproducing-analyses/-/blob/master/one-off/idea.Qmd?ref_type=heads)

#### Reflections on meetings yesterday:
None

## Thematic priorities for today

#### Priority 1
- Make a concrete plan for responding to review comments about integrating PMI and competition
#### Priority 2
- Prepare for meetings with potential grad students
- Draft of lab manual
## Meetings today
- Meet with DBER candidate for breakfast and seminar
- meet Maddi to discuss research

## Tasks I *need* to finish today


## Tasks that would be *nice to* to finish today
- Send Neha response to Biograds email

## Tasks on my radar that are unlikely to happen today
- Write Richard evaluation summary to share with him
- Work on table for plant-microbe interactions global change project
- Send Richard comments on fire effects abstract

## Wishes for today
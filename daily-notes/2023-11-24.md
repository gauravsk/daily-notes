#### Hi! Today is Friday, 24 November

------------------------

## Reflections on yesterday
#### How I spent time yesterday
- Relaxed - Thanksgiving!
- Much-needed cleanup of the lab fridge - should be in good shape now. (This was also relaxing - cleaning therapy I guess)
- Needed a break and couldn't get to work on the Vicks application - need to focus on that now
- Took care of some stray tasks for the Ecology course

#### Reflections on meetings yesterday:
- none

#### Links/Emails/Thoughts from yesterday to follow up on:
- none I think

## Thematic priorities for today

#### Priority 1
- Vicks application

#### Priority 2
- Go on a run, stay healthy

## Meetings today
- Morning meeting with Ameya

## Tasks I *need* to finish today


## Tasks that would be *nice to* to finish today
- Draft of the Vicks application, or at least 2-3 specific activity sections.

## Tasks on my radar that are unlikely to happen today
- writing the paper Review still remains on the to-do list - need to do this by **Monday** at the latest! try to do it over the weekend. 
- Need to decide about remaining Desk order by Sunday 

## Wishes for today
#### Hi! Today is Tuesday, 28 May

------------------------
## Thematic priorities for today

#### Priority 1
Catch up on stuff accumulated over the past week

#### Priority 2
Start on grant writing

## Meetings today
none

## Tasks I *need* to finish today
- [x] Contact summer TAs to meet later this week
- Letter for Jordyn [today or tomorrow]
- [x] Letter for Madelyn [today or tomorrow]
- [x] Letter for Luke [today or tomorrow]


## Tasks that would be *nice to* to finish today

- [x] Reply to Pitman email
## Tasks on my radar that are unlikely to happen today


## Wishes for today
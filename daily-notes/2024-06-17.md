#### Hi! Today is Monday, 17 June

------------------------

## Thematic priorities for today

#### Priority 1
- Set up Alex in lab

#### Priority 2
- Finish review #2 
- Purchasing (laptop, balances, monitors)

## Meetings today
Summer TAs @ 1pm

## Tasks I *need* to finish today


## Tasks that would be *nice to* to finish today
- Purchase 3x laptops
- 

## Tasks on my radar that are unlikely to happen today


## Wishes for today
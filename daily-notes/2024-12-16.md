#### Hi! Today is Monday, 16 December

------------------------

## Reflections on yesterday
#### How I spent time yesterday

#### Reflections on meetings yesterday:

#### Links/Emails/Thoughts from yesterday to follow up on:


## Thematic priorities for today

#### Priority 1

#### Priority 2

## Meetings today


## Tasks I *need* to finish today


## Tasks that would be *nice to* to finish today


## Tasks on my radar that are unlikely to happen today


## Wishes for today
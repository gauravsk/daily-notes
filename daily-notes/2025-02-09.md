#### Hi! Today is Sunday, 9 February

------------------------

## Tasks

### Past due

```tasks
not done
due before today
```

### Due today

```tasks
not done
due on 2025-02-09
sort by function task.file.folder
```


### Due soon

```tasks
not done
due after today
due before next 7 days
```
### On the docket
`
```tasks
not done
due after this week
```

### Accomplished today

```tasks
done on 2025-02-09
```


- https://github.com/rockmanvnx6/obsidian-zola -- turn obsidian notebook into a netlify website
- The repo above is based on this one https://github.com/ppeetteerrs/obsidian-zola
- Obsidian minimal theme https://minimal.guide/home / https://github.com/kepano/obsidian-minimal
- https://notes.andymatuschak.org
	- "Working with the garage door open"
- https://emilhvitfeldt.com/post/slidecraft-layout/ 
	- cool things to do with qmd slides


### Repro research 

https://sepwww.stanford.edu/oldsep/matt/join/ijDoc/Fig/paper_html/node2.html#SECTION00011000000000000000

https://pmc.ncbi.nlm.nih.gov/articles/PMC10666927/  Eleven strategies for making reproducible research and open science training the norm at research institutions

https://academic.oup.com/jalm/article/4/3/471/5636941  # Reproducible Research and Reports with R

https://hdsr.mitpress.mit.edu/pub/fgpmpj1l/release/5

https://rdatatoolbox.github.io/

https://svenbuerki.github.io/EEB603_Reproducible_Science

http://www.practicereproducibleresearch.org/

https://r-cubed-intermediate.rostools.org/sessions/introduction

https://melindahiggins2000.github.io/SNRS2018_ReproducibleResearch/module_01.html#6

http://svmiller.com/blog/2020/04/reinhart-rogoff-ten-years-later-replication/

https://benmarwick.github.io/aswr/dataorganisation.html

https://www.britishecologicalsociety.org/wp-content/uploads/2017/12/guide-to-reproducible-code.pdf

https://r-cubed-intro.rostools.org/

https://ees3310.jgilligan.org/files/lab_docs/lab_01/reproducible_research.pdf

https://fivethirtyeight.com/features/how-two-grad-students-uncovered-michael-lacour-fraud-and-a-way-to-change-opinions-on-transgender-rights/
- Switch to a Tu/Th schedule!!
  
- Weekly reflections was a bit much for me to keep up with, and for the students to do -- many liked it, but some portion started to think of them as busy work. 

- Similarly, weekly activities of 5 points isn't ideal either
  
- So, switching the self reflection and activities to be every-other-week seems like a better schedule.
  
- The students didn't have much background in reading scientific papers and definitely struggled with these activities. It is a good thing to struggle with, but without more structure built into the in-class activities, it may not be quite as effective. 

- It would be good to make the Ecosystem ecology section more quantitative as well -- right now it was a bit too descriptive, which isn't super helpful
  
- Would be good to include readings, but the struggle is that there isn't an existing textbook or repository that I love, that teaches this stuff in a way I find relevant. 
  
- I think I should restructure to keep all the content about creativity and ecology contained within a week - right now it is a bit scatterered, and may lose the punch
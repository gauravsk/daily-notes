https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003897
- " Computational Thinking in Life Science Education"
- To examine the effect of the course on how students view computer science, they were asked to define this discipline before and after the course. Prior to the course, students related the field mostly to the computer as a machine and to software and tools. At the end of the course, however, they tended to relate CS to broader and more abstract terms, such as problem solving and modeling (see [Figure 2](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003897#pcbi-1003897-g002)). We believe this shift in the view of the discipline, especially considering the prior exposure of our students to programming, strengthens the rationale for such a course.

https://www.lifescied.org/doi/10.1187/cbe.16-10-0301
- "Development of a Biological Science Quantitative Reasoning Exam (BioSQuaRE)"


https://www.molbiolcell.org/doi/pdf/10.1091/mbc.e14-06-1045
Teaching quantitative biology: goals, assessments, and resources
- We also posit that engendering positive student attitudes about quantitative work is important, because students’ interests and values, as well as their emotional responses, such as anxiety or enjoyment, can have significant effects on their willingness to engage in learning activities, persevere when they face difficulties, and persist in certain educational or career paths (Steiner and Sullivan, 1984; Glynn et al., 2007; Rheinlander and Wallace, 2011; Matthews et al., 2013; Poladian, 2013). Finally, we anticipate that positive skill and attitudinal outcomes will likely lead to desirable behavioral outcomes, such as enrollment in additional quantitative courses, completion of more quantitative degree programs, and pursuit of further education or careers in quantitative biology


https://pubmed.ncbi.nlm.nih.gov/30100952/
"Life Science Majors' Math-Biology Task Values Relate to Student Characteristics and Predict the Likelihood of Taking Quantitative Biology Courses"
- Students on average reported some cost associated with doing mathematics in biology; however, **they also reported high utility value and were more interested in using mathematics to understand biology than previously believed**. Women and first-generation students reported more negative math-biology task values than men and continuing-generation students.
- there are significant gaps in our understanding of the relationships among math-biology task values, academic-related achievement/choices, and student characteristics, and these gaps inhibit our ability to design effective quantitative instructional materials for biology courses that would meet the needs of diverse learners.


https://www.lifescied.org/doi/pdf/10.1187/cbe.21-07-0180
-"Biology Students’ Math and Computer Science Task Values Are Closely Linked"
- The survey contained both the original Math-Biology Values Instrument (MBVI; Andrews et al., 2017) and a version adapted for CS
- Math and CS values exhibited positive correlations, but **utility and cost were more negative for CS,** possibly due to less exposure to CS before college, and overall attitudes were influenced by CS background and gender

### equity gaps in quantitative/computational biology

- https://blog.google/outreach-initiatives/education/racial-and-gender-gaps-computer-science-learning-new-google-gallup-research/
- https://services.google.com/fh/files/misc/diversity-gaps-in-computer-science-report.pdf
	- "Black students are less likely than White students to have classes dedicated to CS at the school they attend (47% vs. 58%, respectively)."
	- Black (58%) and Hispanic (50%) students are less likely than White students (68%) to use a computer at home at least most days of the week
	- Female students are less likely than male students to be aware of CS learning opportunities on the Internet and in their community, to say they have ever learned CS, and to say they are very interested in learning CS.
- https://www.nature.com/articles/d41586-022-03251-0
	- "Black and Hispanic people face huge hurdles at technology companies and in computer-science education in the United States, with far-reaching consequences for science and all of society."
	- Black and Hispanic people make up almost 13% and 18% of the US workforce, but they hold only 7% and 8%, respectively, of the jobs in computing
	- **The diversity gap is growing at universities**. For bachelor’s degrees, the primary degree granted by US universities, the proportion of computer-science degrees going to Black students has dropped from more than 11% in 2013 to less than 9% in 2020 (see ‘Computing losses’).
	- Hrabowski says structural racism in academia hinders the nation’s ability to deal with some of its biggest challenges. To remedy or even mediate tech’s diversity problem, he says, **academics and stakeholders must first acknowledge and then confront bias in all aspects, from university admissions to recruitment, hiring and promotion**.
	- Mickens and Hrabowski point to the significance of creating a supportive community and matching students from under-represented backgrounds with study partners

- https://www.nature.com/articles/s43588-020-00013-8
	- "Connecting Black women in computational biology"
	- https://www.blackwomencompbio.org/connect

- https://subjectguides.lib.neu.edu/bioinformatics/diversity
- https://www.pnas.org/doi/10.1073/pnas.2117831119#sec-2
- https://www.pewresearch.org/science/2021/04/01/stem-jobs-see-uneven-progress-in-increasing-gender-racial-and-ethnic-diversity/


https://www.jstor.org/stable/j.ctv2bfhj12 Microbial Life History: The Fundamental Forces of Biological Design

https://www.jstor.org/cover-image/j.ctt7ztq5q Ethnobiological Classification: Principles of Categorization of Plants and Animals in Traditional Societies

https://www.jstor.org/cover-image/j.ctvc775wc Monarchs and Milkweed: A Migrating Butterfly, a Poisonous Plant, and Their Remarkable Story of Coevolution

https://www.jstor.org/stable/j.ctvx5w9ws Plant Strategies and the Dynamics and Structure of Plant Communities. (MPB-26), Volume 26

https://www.jstor.org/stable/j.ctt7rmvb Seeds of Amazonian Plants

https://www.jstor.org/stable/j.ctvx5wbgf Evolutionary Ecology across Three Trophic Levels: Goldenrods, Gallmakers, and Natural Enemies (MPB-29)

https://www.jstor.org/stable/j.ctt24hq6n Handbook of Meta-analysis in Ecology and Evolution

https://www.jstor.org/stable/j.ctt6wpzz6 How to Do Ecology: A Concise Handbook - Second Edition

https://www.jstor.org/stable/j.ctt7rkcn Mathematics in Nature: Modeling Patterns in the Natural World
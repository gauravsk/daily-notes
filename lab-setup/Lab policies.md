#### Ideas for establishing lab policies 

- Have a code of conduct discussion with students next semester
- Streamlined procedure for bringing on undergrad students - avoid ad hoc conversations
- Encouraging open science practices among students 
	- Lead lab meetings on what open science is, important tools
- Lab safety protocols
- Make discord access available on lab website
- Implement semester-ly checkins as in Sullivan lab
- Lab meeting policy
- Lab equipment checkout sheet
- Emergency contact sheet
- Field safety
- Add the self reflection/evaluation document to lab website


### Useful links

- https://wiki.weecology.org/docs/
- https://github.com/BahlaiLab/Policies
- https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1006914
- https://elifesciences.org/articles/88853
- https://direct.mit.edu/jocn/article/35/1/44/113591/10-Simple-Rules-for-a-Supportive-Lab-Environment
- https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1010673
- 
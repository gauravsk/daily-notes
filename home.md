
## Welcome. 

This is my sandbox, where I tinker around to try new things, and keep myself honest about the work I have to do in the short- and long-term.

## Daily notes TOC
### November 2023

##### Week 4 
- [[2023-11-21]]
- [[2023-11-22]]
- [[2023-11-23]]
- [[2023-11-24]]
##### Week 5
- [[2023-11-27]]
- [[2023-11-28]]
- [[2023-11-29]]
- [[2023-11-30]]
### December 2023
##### Week 1
- [[2023-12-01]]
- [[2023-12-02]]
- [[2023-12-04]]
- [[2023-12-05]]
- [[2023-12-06]]
##### Week 2
- [[2023-12-11]]
- [[2023-12-15]]
##### Week 3
- [[2023-12-20]]

## 2024

### January 2024
##### Week 1
- [[2024-01-03]]
- [[2024-01-04]]
- [[2024-01-05]]
##### Week 2
- [[2024-01-08]]
- [[2024-01-09]]
### February 2024


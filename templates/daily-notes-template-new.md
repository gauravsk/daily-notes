#### Hi! Today is {{date:dddd\, D MMMM}}

------------------------

## Reflections from yesterday

## Reflections for today

### *Thematic priorities* 

### *Meetings*


## Tasks

### Past due

```tasks
not done
due before today
```

### Due today

```tasks
not done
due on {{date:YYYY-MM-DD}}
sort by function task.file.folder
```


### Due soon

```tasks
not done
due after today
due before next 7 days
```
### On the docket
`
```tasks
not done
due after this week
```

### Accomplished today

```tasks
done on {{date:YYYY-MM-DD}}
```

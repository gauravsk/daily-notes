- [x] #task #sculptors Complete draft of data management plan 📅 2025-02-08 ✅ 2025-02-07
- [x] #task #sculptors Complete draft of mentoring plan 📅 2025-02-11 ✅ 2025-02-16
- [x] #task #sculptors Complete draft of field safety plan 📅 2025-02-11 ✅ 2025-02-21
- [ ] #task #sculptors Finalize budget document 📅 2025-02-26
- [ ] #task #sculptors Reach out to OSP about submitting this grant 📅 2025-02-25
- [x] #task #sculptors Email Calvin to about hoophouse due 📅 2025-02-04 ✅ 2025-02-04
- [ ] #task #sculptors email Caleb about budget question 📅 2025-02-25
- [x] #task #sculptors complete aim 3 text 📅 2025-02-06 ✅ 2025-02-07
- [ ] #task #purchases ELKO - cancel order for 1000 micron and get more 30 micron
- [ ] #task email about Rohit general 📅 2025-02-24
- [ ] 



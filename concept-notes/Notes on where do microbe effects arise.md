The Bever 1997 model assumes that microbes primarily affect plant population dynamics by altering plants' intrinsic (exponential) growth rate:

![[Pasted image 20240519144145.png]]

Downstream metrics like $I_S$ etc. are built on these defintions of $m_{ij}$. 

But, empirical applications of this framework seldom (never??) test whether microbes do in fact affect exponential growth rates, or if the effects are primarily manifesting in some other ways (e.g. mutualistic microbes might just change maximum biomass in a pot rather than the rate of accumulation per se).

We could test where microbial effects manifest in a fairly simple experiment:

For a few focal species, grow plants in sterile/unconditioned/conditioned inoculum
- We can just collect soil under field individuals of different species and omit a conditioning stage entirely, to save time. Otherwise, we can condition soils ourselves
- Take weekly harvests of the pots, or non-destructive measurements
- Fit data to different models of biomass growth
	- Exponential as the "null" model: log(biomass) ~ time * species * soil
	- Alternatively, if an exponential model is not a good fit, try alternatives like linear growth or other. 
- Potential takeaways:
	- Timing of PSF studies should matter: when to end?
	- Ending should happen in the exponential growth phase if we want to fit the Bever model with it


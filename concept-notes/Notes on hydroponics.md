### Project overview to share with Luis

5 January 2024

**Big picture goal:** 
Microbes in the soil can affect various processes in plant communities, and there's a lot of evidence that these effects are important in the biodiversity dynamics of North American tallgrass prairies. For example, soil microbes can contribute to (or detract from) species coexistence in intact/late-successional prairies; they can promote (or hinder) the success of habitat restorations; they can promote species invasions or help buffer communities from invasives, etc.

One of my big-picture goals is to combine mathematical modeling with controlled experiments (field, greenhouse, ... etc) to quantify the effect of soil microbes on plant species coexistence. There are various experimental methods that one can use for studying these interactions, but the general idea is that you measure the performance (e.g. biomass accumulation; survival; germination rates) of individual plants single plants (or in some cases, multiple plants competing in one pot)  that are growing with soil microbial communities of a "known" origin. (E.g. How well does Species A grow with soil microbes from another Species A individual, versus when it grows with soil microbes from an individual of Species B).

**Current focus**
In the next 2-4 years, I hope to submit an NSF grant to evaluate the role of microbes in determining plant species coexistence in tallgrass prairies. I have ideas for what questions I want to address, but my roadblocks are that (a) I don't know the flora of tallgrass prairies (or other grassland habitats in Louisiana) well enough to know which species are conducive to experimental manipulation, and (b) I need to figure out the practical aspects of where to grow the plants, etc.

My goal this semester is to start addressing both roadblocks. I have purchased seeds for ~15 species that are native to tallgrass prairies in Louisiana/Texas, and which have been previously used in similar experiments in Texas. One immediate goal is to find somewhere to plant these seeds so that we can start getting baseline estimates of their germination rate, how fast they grow, what is the best time to plant the seeds, etc. I will be exploring ways to do experiments at Innovation Park, Ben Hur, or other field settings.

But, I am also very eager to explore the possibility of an "in-house" experimental setup that can be used for targetted experiments. To help me learn how an in-house plant growth system might play out for this, I am interested in growing replicate individuals of the ~12-15 species for which I have seeds and see how the species "behave" under these conditions (see species list at the bottom of this note). 

**Potential setup**
For now, the experimental setup doesn't have to be complicated - I am OK doing a pilot study, so that it helps design more intricate studies in the future. But I am open to doing a "real" experiment as well and have some ideas for low-hanging fruits we can explore.

There are (at least) a few small adjustments that we would have to make, compared to your  hydroponic setup described in the Scientific Reports paper. 

- As I understand, in your setup there were large trays of hydroponic solution, and multiple pots per tray. We can't do this for the microbial experiment, because it opens the possibility that microbes from different treatments can "travel" across pots through the water; instead, we would have to isolate each pot in its own small tray. 
  
- Instead of using styrofoam cups, I will have to use plastic pots (which can be autoclaved for sterilizing; the potting soil mix will also be autoclaved)
  
- The potting medium in the pots will not only be sand/vermiculite, but would include some amount of soil from the field. The ratio is TBD, but it could be as much as 50:50 field soil:sand/vermiculite (shown at a lower ratio in the diagram below, for illustration only). In some cases the soil would be "fresh" from the field; in other cases, it may be autoclaved field soil.

- Since the growing medium will include field soil (which could bring with it some non-zero nutrient levels), I wonder if we will want to adjust the hydroponic solution accordingly. 
	- One extreme would be to just use distilled water (no nutrients supplied; this is essentially just bottom-watering the pots); but we can perhaps use this setup as a clean way to manipulate nutrient levels of the soil. 
 
![[pics/hydroponic-schematic.png]]



**Note**: To be honest, I am not entirely clear whether this still counts as a "hydroponic" system, since we clearly have the soil and potting mix as part of the system. So, I wonder if this is just a case of 'persistent bottom watering (and potentially fertilizing)'  rather than a true "hydroponic" setup? 

**Questions**
- How concerned do we need to be about oxygen levels in the water? There will be plenty of space in the pots that is not water-saturated, but would the water tray also need aeration in some way?
	- Is it better to just leave plants in water trays occasionally (e.g. 1 day/week), and out of the water for the remainder of the time? 
- Logistically, will it be a pain to deal with separate trays per pot for upwards of 250 pots (e.g. if we do 12 species * 2 nutrient levels * 2 soil types (live vs. sterilized field soil) * 6 replicates per treatment)?
	- I seem to have pretty good access to strong undergrad students, so I am not super as concerned about the person-hours needed. 
- At some level, this just feels like a "growth chamber" where we bottom-watering the plants (but without the environmental controls of a growth chamber)... is that a bad way to think about it? 
- From your experience, how fussy do we need to be about the specifics of the LED grow-lights? If the setup from your experiments is not in use I am open to just trying it out to see how well these new species grow, but in thinking about potentially expanding it in the long-term, did you find that you had to do a lot of trial-and-error to find the right setup? (e.g. did you match PPFD to outdoor, etc.)

**Species list**

*Asteraceae*: 
	Rudbeckia hirta (Black-eyed Susan), 
	Solidago rigida (Rigid goldenrod), 
	Echinacea purpurea (Purple coneflower), 
	Ratibida columnifera, 
	Vernonia baldwinii (Western ironweed;
*Verbenaceae*: 
	Glandularia bipinnatifida (Prairie verbena);
*Lamiaceae*: 
	Monarda citriodora (Lemon mint);
*Apocynaceae*:  
	Asclepias tuberosa (butterfly weed), 
	Asclepias viridis (green milkweed);
*Poaceae*: 
	Sorghastrum nutans; 
	Schizachyrium scoparium
*Fabaceae*: 
	Chamaecrista fasciculata (2 different sources for this species);
	Dalea candida

**Other details**

Assuming that we can work with a shelving unit that has racks of 18" by 48" (e.g. [this product](https://www.amazon.com/gp/aw/d/B0BL834CDX/)), we should be able to fit 24 pots on each shelf (using pots like these, [link](https://www.growerssupply.com/prod/pg113618.html)). 

If we want to do a quick-and-dirty setup of just 6 replicate individuals per species, that mean each shelf fits 4 species; across three shelves, we can accommodate a dozen species.  
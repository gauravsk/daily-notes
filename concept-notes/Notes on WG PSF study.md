
### Exploring experiment numbers

##### Feedback experiment 

In this experiment, I am assuming each pot holds 1 plant; only difference is in soil treatment. 

For doing a pairwise PSF experiment of $n$ species, each species has $n+2$ soil treatments (at least); thus, there are a total of $n*(n+2)$ "unique pots" that need to be replicated. So for 5 species, the number is 35 "unique" pot type; for 6 species, it is 48. If we want to replicate this experiment across two environmental treatments, we need 92 unique pots, each of which gets replicated across as many blocks as are reasonable. 

(I'd say for this scale of a study, 10 replicate pots is a good target)

That means that for a 6-species PSF study with two environmental levels (e.g. high/low water), we would need in the ballpark of 1000 pots total, with at least 160 plants per species. 

-------------------------------

##### Microbes X Competition experiment

If we want to do a competition experiment with $n$ species, the number of unique pots required (assuming we want each species to grow alone, with 1 competitor, or with "high density" of competitors) can explode really quickly. For example with just 4 species, we require 30 "unique" combinations of plants, **for each microbial environment!** These are the treatments per microbial environment:

![[comp-density-schematic.png]]

Now we need to scale this up for different microbial environments: 

The Intrinsic Growth and Intraspecific Comp treatments (both low and high) only need to be repeated in Conspecific and Reference soils. The "Interspecific Comp (low density)" treatments need to be repeated in Reference, Sp1, and Sp2 soils. The "Interspecific Comp (high density)" treatments only need to be repeated in Reference soil, and Soil of the "background" (non-focal) species. 

I think the general formula is that we need $$\overbrace{(2_{soils}*3n)}^{\text{intinsic\ and\ intrasp\ comp}} + \overbrace{3_{soils}*{n\choose2}}^{\text{intersp\ comp\ low}} + \overbrace{(2_{soils}*2*{n\choose2})}^{\text{intersp\ comp\ high}}$$
![[Pasted image 20240108165516.png]]
pots for the experiment, multiplied by the number of replicates... That's a lot! For n = 4 species, we need 66 pots per replicate. 

The number of seedlings per species also varies with $n$. For each species, we need:
- 1 seedling for intrinsic growth measurements, 
- 2 seedlings for intrasp comp (low), 
- 1+(background density) seedlings for intrasp comp high,
- n-1 seedlings for intersp comp-low,
- and (n-1)+(n-1)\*(background density) for intersp comp-high. 

So, for $n = 4$ and $\text{background density} = 4$, we would need:
1+2+1+4+3+3+(3\*5) = 29 seedlings per species. This means on the order of 150 usable seedlings for a minimal design of 5 replicate pots per treatment. 

Actually now that it is calculated out, the number doesn't seem too absurd. 


Another issue is the number of seeds per species. I will work on this next. 